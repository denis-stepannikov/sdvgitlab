FROM centos:7
RUN yum -y update && yum -y install python3
RUN pip3 install flask flask-jsonpify flask-restful

WORKDIR /python_api
COPY ./python_api /python_api

CMD ["python3", "python-api.py"]
